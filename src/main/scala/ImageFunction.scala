package etl
import java.io._

import com.itextpdf.text.io.RandomAccessSourceFactory
import com.itextpdf.text.pdf._
import com.itextpdf.text.pdf.codec.TiffImage
import com.itextpdf.text.{Document, PageSize}
import org.apache.spark.input.PortableDataStream
import org.apache.spark.{SparkConf, SparkContext}


object ImageFunction extends App{

  val conf = new SparkConf().setAppName("testApp")
  val sc = new SparkContext(conf)
  val tifsPath = "/opt/ff_test_data/test2/*.tif"


  val toPdf = (name: String, data: PortableDataStream) => {

    val fact = new RandomAccessSourceFactory();

    val tifFile = new RandomAccessFileOrArray(fact.createSource(data.toArray()))

    val pdfName = name.replaceAll("\\.[^.]*$",".pdf").replaceAll("file:","")

    val document = new Document(PageSize.A4, 0,0,0,0);

    PdfWriter.getInstance(document, new FileOutputStream(pdfName))

    document.open()

    val  image = TiffImage.getTiffImage(tifFile,1)
    val scaler = ((document.getPageSize().getWidth() - document.leftMargin()
      - document.rightMargin() ) / image.getWidth()) * 100
    val scaledImage = image.scalePercent(scaler)
    document.add(image)
    document.close()

    (pdfName, document)
  }


  val inputRdd = sc.binaryFiles(tifsPath)
  val s = System.currentTimeMillis


  inputRdd.map(toPdf.tupled).count()
  val elapsed = System.currentTimeMillis() - s
  println("ElapsedTime: ".concat(elapsed.toString))

  // val (infile: String, indata: PortableDataStream) = inputRdd.collect.head

  // Make sure the name and array match
  // assert(infile.contains(outFileName)) // a prefix may get added
  //assert(indata.toArray === testOutput)


  //Read tif bytes and create datastream to pdf bytes
  sc.stop()

}
