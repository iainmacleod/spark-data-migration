lazy val root = (project in file(".")).
  settings(
  name := "image-convert",
  version := "1.0",
  scalaVersion := "2.10.4",
  mainClass in Compile := Some("etl.ImageFunction")
)

libraryDependencies += "com.itextpdf" % "itextpdf" % "5.5.9"
libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.1" % "provided"



